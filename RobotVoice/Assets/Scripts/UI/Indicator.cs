using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class Indicator : MonoBehaviour
    {
        [SerializeField] public Text label;
        [SerializeField] public Text value;
    }
}
